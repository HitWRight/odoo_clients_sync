use xmlrpc::{Value};

#[derive(Debug)]
pub struct Version {
    pub protocol_version: i32,
    pub server_serie: String,
    pub server_version: String,
    pub server_version_info: Value,
}
