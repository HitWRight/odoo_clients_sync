use crate::odoo::*;
use std::collections::BTreeMap;
use xmlrpc::Value;

#[derive(Clone, Debug)]
pub struct Bank {
    id: i32,
    pub name: String,
}

impl Bank {
    pub fn new(name: String) -> Bank {
        Bank { id: 0, name }
    }
}

impl Model for Bank {
    fn get_id(&self) -> i32 {
        self.id
    }
    fn get_table_name() -> &'static str {
        "res.bank"
    }
    fn into_odoo_struct(self) -> Value {
        let mut result = BTreeMap::new();
        result.insert("id".to_string(), Value::Int(self.id));
        result.insert("name".to_string(), Value::String(self.name));
        Value::Struct(result)
    }
    fn parse(value: Value) -> Self {
        if let Value::Struct(value) = value {
            Self {
                id: parse_int(&value, "id"),
                name: parse_string(&value, "name"),
            }
        } else {
            panic!("Incorrect argument passed to parse function");
        }
    }
}
