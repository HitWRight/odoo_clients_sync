// use std::collections::BTreeMap;
use crate::odoo::into_ids;
use crate::odoo::into_option;
use crate::odoo::into_option_string;
use crate::odoo::parse_bool;
use crate::odoo::parse_double;
use crate::odoo::parse_ids;
use crate::odoo::parse_int;
use crate::odoo::parse_option;
use crate::odoo::parse_option_string;
use crate::odoo::parse_string;
use crate::odoo::Model;
use std::collections::BTreeMap;
use xmlrpc::Value;

#[derive(Debug)]
pub struct Partner {
    active: bool,
    active_lang_count: i32,
    activity_date_deadline: bool,
    activity_exception_decoration: bool,
    activity_exception_icon: bool,
    activity_ids: Vec<i32>,
    activity_state: bool,
    activity_summary: bool,
    activity_type_id: bool,
    activity_user_id: bool,
    additional_info: bool,
    pub aurora_id: i32,
    bank_ids: Vec<i32>,
    calendar_last_notif_ack: String,
    category_id: Vec<i32>,
    channel_ids: Vec<i32>,
    child_ids: Vec<i32>,
    city: String,
    color: i32,
    pub comment: Option<String>,
    commercial_company_name: String,
    commercial_partner_id: Option<(i32, String)>,
    company_id: Option<(i32, String)>,
    company_name: Option<String>,
    company_type: String,
    contact_address: String,
    country_id: Option<(i32, String)>,
    create_date: String,
    create_uid: Option<(i32, String)>,
    credit_limit: f64,
    date: bool,
    display_name: String,
    pub email: String,
    email_formatted: String,
    email_normalized: String,
    employee: bool,
    entity_type: bool,
    function: Option<String>,
    id: i32,
    im_status: String,
    // image_1024: String,
    // image_128: String,
    // image_1920: String,
    // image_256: String,
    // image_512: String,
    industry_id: bool,
    is_blacklisted: bool,
    pub is_company: bool,
    lang: Option<String>,
    lead_id: Vec<i32>,
    meeting_count: i32,
    meeting_ids: Vec<i32>,
    message_attachment_count: i32,
    message_bounce: i32,
    message_channel_ids: Vec<i32>,
    message_follower_ids: Vec<i32>,
    message_has_error: bool,
    message_has_error_counter: i32,
    message_has_sms_error: bool,
    message_ids: Vec<i32>,
    message_is_follower: bool,
    message_main_attachment_id: bool,
    message_needaction: bool,
    message_needaction_counter: i32,
    message_partner_ids: Vec<i32>,
    message_unread: bool,
    message_unread_counter: i32,
    mobile: bool,
    name: String,
    opportunity_count: i32,
    opportunity_ids: Vec<i32>,
    parent_id: Option<(i32, String)>,
    parent_name: Option<String>,
    partner_gid: i32,
    partner_latitude: f64,
    partner_longitude: f64,
    partner_share: bool,
    pub phone: String,
    phone_blacklisted: bool,
    phone_sanitized: Option<String>,
    r#ref: bool,
    pub registration_id: Option<String>,
    same_vat_partner_id: bool,
    signup_expiration: bool,
    signup_token: bool,
    signup_type: bool,
    signup_url: Option<String>,
    signup_valid: bool,
    state_id: Option<(i32, String)>,
    street: String,
    street2: bool,
    team_id: bool,
    title: bool,
    r#type: String,
    tz: Option<String>,
    tz_offset: String,
    user_id: bool,
    user_ids: Vec<i32>,
    pub vat: Option<String>,
    pub vat_regression: bool,
    website: Option<String>,
    website_message_ids: Vec<i32>,
    write_date: String,
    write_uid: Option<(i32, String)>,
    zip: String,
}

impl Partner {
    pub fn new(name: String) -> Self {
        Partner {
            active: true,
            active_lang_count: 0,
            activity_date_deadline: false,
            activity_exception_decoration: false,
            activity_exception_icon: false,
            activity_ids: vec![],
            activity_state: false,
            activity_summary: false,
            activity_type_id: false,
            activity_user_id: false,
            additional_info: false,
            aurora_id: 0,
            bank_ids: vec![],
            calendar_last_notif_ack: "1970-01-01".to_string(),
            category_id: vec![],
            channel_ids: vec![],
            child_ids: vec![],
            city: String::new(),
            color: 0,
            comment: None,
            commercial_company_name: String::new(),
            commercial_partner_id: None,
            company_id: None,
            company_name: None,
            company_type: String::new(),
            contact_address: String::new(),
            country_id: None,
            create_date: "1970-01-01".to_string(),
            create_uid: None,
            credit_limit: 0.0,
            date: false,
            display_name: String::new(),
            email: String::new(),
            email_formatted: String::new(),
            email_normalized: String::new(),
            employee: false,
            entity_type: false,
            function: None,
            id: 0,
            im_status: String::new(),
            // image_1024: String::new(),
            // image_128: String::new(),
            // image_1920: String::new(),
            // image_256: String::new(),
            // image_512: String::new(),
            industry_id: false,
            is_blacklisted: false,
            is_company: false,
            lang: None,
            lead_id: vec![],
            meeting_count: 0,
            meeting_ids: vec![],
            message_attachment_count: 0,
            message_bounce: 0,
            message_channel_ids: vec![],
            message_follower_ids: vec![],
            message_has_error: false,
            message_has_error_counter: 0,
            message_has_sms_error: false,
            message_ids: vec![],
            message_is_follower: false,
            message_main_attachment_id: false,
            message_needaction: false,
            message_needaction_counter: 0,
            message_partner_ids: vec![],
            message_unread: false,
            message_unread_counter: 0,
            mobile: false,
            name,
            opportunity_count: 0,
            opportunity_ids: vec![],
            parent_id: None,
            parent_name: None,
            partner_gid: 0,
            partner_latitude: 0.0,
            partner_longitude: 0.0,
            partner_share: false,
            phone: String::new(),
            phone_blacklisted: false,
            phone_sanitized: None,
            r#ref: false,
            registration_id: None,
            same_vat_partner_id: false,
            signup_expiration: false,
            signup_token: false,
            signup_type: false,
            signup_url: None,
            signup_valid: false,
            state_id: None,
            street: String::new(),
            street2: false,
            team_id: false,
            title: false,
            r#type: "contact".to_string(),
            tz: None,
            tz_offset: "+2".to_string(),
            user_id: false,
            user_ids: vec![],
            vat: None,
            vat_regression: false,
            website: None,
            website_message_ids: vec![],
            write_date: "2020-07-22".to_string(),
            write_uid: None,
            zip: String::new(),
        }
    }
}

impl Model for Partner {
    fn parse(value: Value) -> Self {
        if let Value::Struct(value) = value {
            Self {
                active: parse_bool(&value, "active"),
                active_lang_count: parse_int(&value, "active_lang_count"),
                activity_date_deadline: parse_bool(&value, "activity_date_deadline"),
                activity_exception_decoration: parse_bool(&value, "activity_exception_decoration"),
                activity_exception_icon: parse_bool(&value, "activity_exception_icon"),
                activity_ids: parse_ids(&value, "activity_ids"),
                activity_state: parse_bool(&value, "activity_state"),
                activity_summary: parse_bool(&value, "activity_summary"),
                activity_type_id: parse_bool(&value, "activity_type_id"),
                activity_user_id: parse_bool(&value, "activity_user_id"),
                additional_info: parse_bool(&value, "additional_info"),
                aurora_id: parse_int(&value, "aurora_id"),
                bank_ids: parse_ids(&value, "bank_ids"),
                calendar_last_notif_ack: parse_string(&value, "calendar_last_notif_ack"),
                category_id: parse_ids(&value, "category_id"),
                channel_ids: parse_ids(&value, "channel_ids"),
                child_ids: parse_ids(&value, "child_ids"),
                city: parse_string(&value, "city"),
                color: parse_int(&value, "color"),
                comment: parse_option_string(&value, "comment"),
                commercial_company_name: parse_string(&value, "commercial_company_name"),
                commercial_partner_id: parse_option(&value, "commercial_partner_id"),
                company_id: parse_option(&value, "company_id"),
                company_name: parse_option_string(&value, "company_name"),
                company_type: parse_string(&value, "company_type"),
                contact_address: parse_string(&value, "contact_address"),
                country_id: parse_option(&value, "country_id"),
                create_date: parse_string(&value, "create_date"),
                create_uid: parse_option(&value, "create_uid"),
                credit_limit: parse_double(&value, "credit_limit"),
                date: parse_bool(&value, "date"),
                display_name: parse_string(&value, "display_name"),
                email: parse_string(&value, "email"),
                email_formatted: parse_string(&value, "email_formatted"),
                email_normalized: parse_string(&value, "email_normalized"),
                employee: parse_bool(&value, "employee"),
                entity_type: parse_bool(&value, "entity_type"),
                function: parse_option_string(&value, "function"),
                id: parse_int(&value, "id"),
                im_status: parse_string(&value, "im_status"),
                // image_1024: match value
                //     .get("image_1024")
                //     .expect("Failed to find image_1024 in response")
                // {
                //     Value::String(x) => x.clone(),
                //     x => panic!(format!(
                //         "Failed to parseimage_1024, did the structure change? {:?}",
                //         x
                //     )),
                // },
                // image_128: match value
                //     .get("image_128")
                //     .expect("Failed to find image_128 in response")
                // {
                //     Value::String(x) => x.clone(),
                //     x => panic!(format!(
                //         "Failed to parseimage_128, did the structure change? {:?}",
                //         x
                //     )),
                // },
                // image_1920: match value
                //     .get("image_1920")
                //     .expect("Failed to find image_1920 in response")
                // {
                //     Value::String(x) => x.clone(),
                //     x => panic!(format!(
                //         "Failed to parseimage_1920, did the structure change? {:?}",
                //         x
                //     )),
                // },
                // image_256: match value
                //     .get("image_256")
                //     .expect("Failed to find image_256 in response")
                // {
                //     Value::String(x) => x.clone(),
                //     x => panic!(format!(
                //         "Failed to parseimage_256, did the structure change? {:?}",
                //         x
                //     )),
                // },
                // image_512: match value
                //     .get("image_512")
                //     .expect("Failed to find image_512 in response")
                // {
                //     Value::String(x) => x.clone(),
                //     x => panic!(format!(
                //         "Failed to parseimage_512, did the structure change? {:?}",
                //         x
                //     )),
                // },
                industry_id: parse_bool(&value, "industry_id"),
                is_blacklisted: parse_bool(&value, "is_blacklisted"),
                is_company: parse_bool(&value, "is_company"),
                lang: parse_option_string(&value, "lang"),
                lead_id: parse_ids(&value, "lead_id"),
                meeting_count: parse_int(&value, "meeting_count"),
                meeting_ids: parse_ids(&value, "meeting_ids"),
                message_attachment_count: parse_int(&value, "message_attachment_count"),
                message_bounce: parse_int(&value, "message_bounce"),
                message_channel_ids: parse_ids(&value, "message_channel_ids"),
                message_follower_ids: parse_ids(&value, "message_follower_ids"),
                message_has_error: parse_bool(&value, "message_has_error"),
                message_has_error_counter: parse_int(&value, "message_has_error_counter"),
                message_has_sms_error: parse_bool(&value, "message_has_sms_error"),
                message_ids: parse_ids(&value, "message_ids"),
                message_is_follower: parse_bool(&value, "message_is_follower"),
                message_main_attachment_id: parse_bool(&value, "message_main_attachment_id"),
                message_needaction: parse_bool(&value, "message_needaction"),
                message_needaction_counter: parse_int(&value, "message_needaction_counter"),
                message_partner_ids: parse_ids(&value, "message_partner_ids"),
                message_unread: parse_bool(&value, "message_unread"),
                message_unread_counter: parse_int(&value, "message_unread_counter"),
                mobile: parse_bool(&value, "mobile"),
                name: parse_string(&value, "name"),
                opportunity_count: parse_int(&value, "opportunity_count"),
                opportunity_ids: parse_ids(&value, "opportunity_ids"),
                parent_id: parse_option(&value, "parent_id"),
                parent_name: parse_option_string(&value, "parent_name"),
                partner_gid: parse_int(&value, "partner_gid"),
                partner_latitude: parse_double(&value, "partner_latitude"),
                partner_longitude: parse_double(&value, "partner_longitude"),
                partner_share: parse_bool(&value, "partner_share"),
                phone: parse_string(&value, "phone"),
                phone_blacklisted: parse_bool(&value, "phone_blacklisted"),
                phone_sanitized: parse_option_string(&value, "phone_sanitized"),
                r#ref: parse_bool(&value, "ref"),
                registration_id: parse_option_string(&value, "registration_id"),
                same_vat_partner_id: parse_bool(&value, "same_vat_partner_id"),
                signup_expiration: parse_bool(&value, "signup_expiration"),
                signup_token: parse_bool(&value, "signup_token"),
                signup_type: parse_bool(&value, "signup_type"),
                signup_url: parse_option_string(&value, "signup_url"),
                signup_valid: parse_bool(&value, "signup_valid"),
                state_id: parse_option(&value, "state_id"),
                street: parse_string(&value, "street"),
                street2: parse_bool(&value, "street2"),
                team_id: parse_bool(&value, "team_id"),
                title: parse_bool(&value, "title"),
                r#type: parse_string(&value, "type"),
                tz: parse_option_string(&value, "tz"),
                tz_offset: parse_string(&value, "tz_offset"),
                user_id: parse_bool(&value, "user_id"),
                user_ids: parse_ids(&value, "user_ids"),
                vat: parse_option_string(&value, "vat"),
                vat_regression: parse_bool(&value, "vat_regression"),
                website: parse_option_string(&value, "website"),
                website_message_ids: parse_ids(&value, "website_message_ids"),
                write_date: parse_string(&value, "write_date"),
                write_uid: parse_option(&value, "write_uid"),
                zip: parse_string(&value, "zip"),
            }
        } else {
            panic!("Incorrect argument passed to parse function");
        }
    }

    fn into_odoo_struct(self) -> Value {
        let mut result = BTreeMap::new();

        result.insert("active".to_string(), Value::Bool(self.active));
        result.insert(
            "active_lang_count".to_string(),
            Value::Int(self.active_lang_count),
        );
        result.insert(
            "activity_date_deadline".to_string(),
            Value::Bool(self.activity_date_deadline),
        );
        result.insert(
            "activity_exception_decoration".to_string(),
            Value::Bool(self.activity_exception_decoration),
        );
        result.insert(
            "activity_exception_icon".to_string(),
            Value::Bool(self.activity_exception_icon),
        );
        result.insert("activity_ids".to_string(), into_ids(self.activity_ids));
        result.insert(
            "activity_state".to_string(),
            Value::Bool(self.activity_state),
        );
        result.insert(
            "activity_summary".to_string(),
            Value::Bool(self.activity_summary),
        );
        result.insert(
            "activity_type_id".to_string(),
            Value::Bool(self.activity_type_id),
        );
        result.insert(
            "activity_user_id".to_string(),
            Value::Bool(self.activity_user_id),
        );
        result.insert(
            "additional_info".to_string(),
            Value::Bool(self.additional_info),
        );
        result.insert("aurora_id".to_string(), Value::Int(self.aurora_id));
        result.insert("bank_ids".to_string(), into_ids(self.bank_ids));
        result.insert(
            "calendar_last_notif_ack".to_string(),
            Value::String(self.calendar_last_notif_ack),
        );
        result.insert("category_id".to_string(), into_ids(self.category_id));
        result.insert("channel_ids".to_string(), into_ids(self.channel_ids));
        result.insert("child_ids".to_string(), into_ids(self.child_ids));
        result.insert("city".to_string(), Value::String(self.city));
        result.insert("color".to_string(), Value::Int(self.color));
        result.insert("comment".to_string(), into_option_string(self.comment));
        result.insert(
            "commercial_company_name".to_string(),
            Value::String(self.commercial_company_name),
        );
        result.insert(
            "commercial_partner_id".to_string(),
            into_option(self.commercial_partner_id),
        );
        result.insert("company_id".to_string(), into_option(self.company_id));
        result.insert(
            "company_name".to_string(),
            into_option_string(self.company_name),
        );
        result.insert("company_type".to_string(), Value::String(self.company_type));
        result.insert(
            "contact_address".to_string(),
            Value::String(self.contact_address),
        );
        result.insert("country_id".to_string(), into_option(self.country_id));
        result.insert("create_date".to_string(), Value::String(self.create_date));
        result.insert("create_uid".to_string(), into_option(self.create_uid));
        result.insert("credit_limit".to_string(), Value::Double(self.credit_limit));
        result.insert("date".to_string(), Value::Bool(self.date));
        result.insert("display_name".to_string(), Value::String(self.display_name));
        result.insert("email".to_string(), Value::String(self.email));
        result.insert(
            "email_formatted".to_string(),
            Value::String(self.email_formatted),
        );
        result.insert(
            "email_normalized".to_string(),
            Value::String(self.email_normalized),
        );
        result.insert("employee".to_string(), Value::Bool(self.employee));
        result.insert("entity_type".to_string(), Value::Bool(self.entity_type));
        result.insert("function".to_string(), into_option_string(self.function));
        result.insert("id".to_string(), Value::Int(self.id));
        result.insert("im_status".to_string(), Value::String(self.im_status));
        // result.insert("// image_1024".to_string(), Value::String(self.// image_1024));
        // result.insert("// image_128".to_string(), Value::String(self.// image_128));
        // result.insert("// image_1920".to_string(), Value::String(self.// image_1920));
        // result.insert("// image_256".to_string(), Value::String(self.// image_256));
        // result.insert("// image_512".to_string(), Value::String(self.// image_512));
        result.insert("industry_id".to_string(), Value::Bool(self.industry_id));
        result.insert(
            "is_blacklisted".to_string(),
            Value::Bool(self.is_blacklisted),
        );
        result.insert("is_company".to_string(), Value::Bool(self.is_company));
        result.insert("lang".to_string(), into_option_string(self.lang));
        result.insert("lead_id".to_string(), into_ids(self.lead_id));
        result.insert("meeting_count".to_string(), Value::Int(self.meeting_count));
        result.insert("meeting_ids".to_string(), into_ids(self.meeting_ids));
        result.insert(
            "message_attachment_count".to_string(),
            Value::Int(self.message_attachment_count),
        );
        result.insert(
            "message_bounce".to_string(),
            Value::Int(self.message_bounce),
        );
        result.insert(
            "message_channel_ids".to_string(),
            into_ids(self.message_channel_ids),
        );
        result.insert(
            "message_follower_ids".to_string(),
            into_ids(self.message_follower_ids),
        );
        result.insert(
            "message_has_error".to_string(),
            Value::Bool(self.message_has_error),
        );
        result.insert(
            "message_has_error_counter".to_string(),
            Value::Int(self.message_has_error_counter),
        );
        result.insert(
            "message_has_sms_error".to_string(),
            Value::Bool(self.message_has_sms_error),
        );
        result.insert("message_ids".to_string(), into_ids(self.message_ids));
        result.insert(
            "message_is_follower".to_string(),
            Value::Bool(self.message_is_follower),
        );
        result.insert(
            "message_main_attachment_id".to_string(),
            Value::Bool(self.message_main_attachment_id),
        );
        result.insert(
            "message_needaction".to_string(),
            Value::Bool(self.message_needaction),
        );
        result.insert(
            "message_needaction_counter".to_string(),
            Value::Int(self.message_needaction_counter),
        );
        result.insert(
            "message_partner_ids".to_string(),
            into_ids(self.message_partner_ids),
        );
        result.insert(
            "message_unread".to_string(),
            Value::Bool(self.message_unread),
        );
        result.insert(
            "message_unread_counter".to_string(),
            Value::Int(self.message_unread_counter),
        );
        result.insert("mobile".to_string(), Value::Bool(self.mobile));
        result.insert("name".to_string(), Value::String(self.name));
        result.insert(
            "opportunity_count".to_string(),
            Value::Int(self.opportunity_count),
        );
        result.insert(
            "opportunity_ids".to_string(),
            into_ids(self.opportunity_ids),
        );
        result.insert("parent_id".to_string(), into_option(self.parent_id));
        result.insert(
            "parent_name".to_string(),
            into_option_string(self.parent_name),
        );
        result.insert("partner_gid".to_string(), Value::Int(self.partner_gid));
        result.insert(
            "partner_latitude".to_string(),
            Value::Double(self.partner_latitude),
        );
        result.insert(
            "partner_longitude".to_string(),
            Value::Double(self.partner_longitude),
        );
        result.insert("partner_share".to_string(), Value::Bool(self.partner_share));
        result.insert("phone".to_string(), Value::String(self.phone));
        result.insert(
            "phone_blacklisted".to_string(),
            Value::Bool(self.phone_blacklisted),
        );
        result.insert(
            "phone_sanitized".to_string(),
            into_option_string(self.phone_sanitized),
        );
        result.insert("ref".to_string(), Value::Bool(self.r#ref));
        result.insert(
            "registration_id".to_string(),
            into_option_string(self.registration_id),
        );
        result.insert(
            "same_vat_partner_id".to_string(),
            Value::Bool(self.same_vat_partner_id),
        );
        result.insert(
            "signup_expiration".to_string(),
            Value::Bool(self.signup_expiration),
        );
        result.insert("signup_token".to_string(), Value::Bool(self.signup_token));
        result.insert("signup_type".to_string(), Value::Bool(self.signup_type));
        result.insert(
            "signup_url".to_string(),
            into_option_string(self.signup_url),
        );
        result.insert("signup_valid".to_string(), Value::Bool(self.signup_valid));
        result.insert("state_id".to_string(), into_option(self.state_id));
        result.insert("street".to_string(), Value::String(self.street));
        result.insert("street2".to_string(), Value::Bool(self.street2));
        result.insert("team_id".to_string(), Value::Bool(self.team_id));
        result.insert("title".to_string(), Value::Bool(self.title));
        result.insert("type".to_string(), Value::String(self.r#type));
        result.insert("tz".to_string(), into_option_string(self.tz));
        result.insert("tz_offset".to_string(), Value::String(self.tz_offset));
        result.insert("user_id".to_string(), Value::Bool(self.user_id));
        result.insert("user_ids".to_string(), into_ids(self.user_ids));
        result.insert("vat".to_string(), into_option_string(self.vat));
        result.insert(
            "vat_regression".to_string(),
            Value::Bool(self.vat_regression),
        );
        result.insert("website".to_string(), into_option_string(self.website));
        result.insert(
            "website_message_ids".to_string(),
            into_ids(self.website_message_ids),
        );
        result.insert("write_date".to_string(), Value::String(self.write_date));
        result.insert("write_uid".to_string(), into_option(self.write_uid));
        result.insert("zip".to_string(), Value::String(self.zip));
        // let result = Value::Struct::new();
        Value::Struct(result)
    }
    fn get_table_name() -> &'static str {
        "res.partner"
    }
    fn get_id(&self) -> i32 {
        self.id
    }
}
