use crate::odoo::Model;
use std::collections::BTreeMap;
use std::time::Instant;
use url::Url;
use xmlrpc::{Request, Value};

const PATH: &str = "xmlrpc/2/object";

#[derive(Debug)]
pub struct Authenticator {
    url: Url,
    db: String,
    uid: i32,
    pass: String,
}

impl Authenticator {
    pub fn new(url: Url, db: &str, uid: i32, pass: &str) -> Authenticator {
        Authenticator {
            url,
            db: db.to_string().clone(),
            uid,
            pass: pass.to_string().clone(),
        }
    }

    pub fn search_read<T>(&self, filter: Value) -> Vec<T>
    where
        T: Model,
    {
        let mut result = Vec::<T>::new();
        let limits = BTreeMap::<String, Value>::new();
        let now = Instant::now();

        // let filter = Value::Array(vec![
        //     // Value::Array(vec![Value::Array(vec![
        //     // Value::String("is_company".to_string()),
        //     // Value::String("=".to_string()),
        //     // Value::Bool(true),
        //     // ])])
        // ]);

        let search_request = Request::new("execute_kw")
            .arg(self.db.clone())
            .arg(self.uid)
            .arg(self.pass.clone())
            .arg(T::get_table_name())
            .arg("search_read")
            .arg(filter)
            .arg(Value::Struct(limits));
        let search_result = search_request
            .call_url(self.url.join(PATH).unwrap())
            .expect("Failed to retrieve results");
        // println!("{:?}", search_result);
        if let Value::Array(search_result) = search_result {
            for a in search_result {
                let item = T::parse(a);
                result.push(item);
            }
        }

        println!(
            "Item retrieved count: {} time taken: {:?}",
            result.len(),
            now.elapsed().as_millis()
        );
        // println!("{:#?}", result);
        result
    }

    pub fn create<T: crate::odoo::Model>(&self, item: T) -> () {
        let create_request = Request::new("execute_kw")
            .arg(self.db.clone())
            .arg(self.uid)
            .arg(self.pass.clone())
            .arg(T::get_table_name())
            .arg("create")
            .arg(Value::Array(vec![item.into_odoo_struct()]));
        let create_result = create_request
            .call_url(self.url.join(PATH).unwrap())
            .expect("failed to create item");
        println!("Item created {:#?}", create_result);
    }

    pub fn delete<T: crate::odoo::Model>(&self, item: T) -> (){
        let unlink_request = Request::new("execute_kw")
            .arg(self.db.clone())
            .arg(self.uid)
            .arg(self.pass.clone())
            .arg(T::get_table_name())
            .arg("unlink")
            .arg(Value::Array(vec![Value::Array(vec![
                Value::Int(item.get_id())
            ])]));

        let unlink_result = unlink_request.call_url(self.url.join(PATH).unwrap()).expect("Failed to remove item");
        println!("Item removed {:#?}", unlink_result);
    }

    pub fn write<T: crate::odoo::Model>(&self, item: T) -> () {
        let write_request = Request::new("execute_kw")
            .arg(self.db.clone())
            .arg(self.uid)
            .arg(self.pass.clone())
            .arg(T::get_table_name())
            .arg("write")
            .arg(Value::Array(vec![
                Value::Array(vec![Value::Int(item.get_id())]),
                item.into_odoo_struct(),
            ]));
        let write_result = write_request.call_url(self.url.join(PATH).unwrap()).expect("Failed to write item");
        println!("Write result ({:#?})", write_result);
    }
}
