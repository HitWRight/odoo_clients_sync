pub mod authenticator;
pub mod connector;
pub mod models;

use std::collections::BTreeMap;
use xmlrpc::Value;

pub fn parse_bool(value: &BTreeMap<String, Value>, name: &str) -> bool {
    match value
        .get(name)
        .expect(&format!("Failed to find {} in response", name))
    {
        Value::Bool(x) => *x,
        x => panic!(format!(
            "Failed to {}, did the structure change? {:?}",
            name, x
        )),
    }
}
pub fn parse_double(value: &BTreeMap<String, Value>, name: &str) -> f64 {
    match value
        .get(name)
        .expect(&format!("Failed to find {} in response", name))
    {
        Value::Double(x) => *x,
        x => panic!(format!(
            "Failed to {}, did the structure change? {:?}",
            name, x
        )),
    }
}
pub fn parse_ids(value: &BTreeMap<String, Value>, name: &str) -> Vec<i32> {
    match value
        .get(name)
        .expect(&format!("Failed to find {} in response", name))
    {
        Value::Array(x) => x
            .into_iter()
            .map(|id| match id {
                Value::Int(x) => *x,
                x => panic!(format!(
                    "Failed to parse {}, did the structure change? {:?}",
                    name, x
                )),
            })
            .collect(),
        x => panic!(format!(
            "Failed to parse {}, did the structure change? {:?}",
            name, x
        )),
    }
}
pub fn parse_int(value: &BTreeMap<String, Value>, name: &str) -> i32 {
    match value
        .get(name)
        .expect(&format!("Failed to find {} in response", name))
    {
        Value::Int(x) => *x,
        x => panic!(format!(
            "Failed to {}, did the structure change? {:?}",
            name, x
        )),
    }
}
pub fn parse_option(value: &BTreeMap<String, Value>, name: &str) -> Option<(i32, String)> {
    match value
        .get(name)
        .expect(&format!("Failed to find {} in response", name))
    {
        Value::Array(x) => match &x[..] {
            [] => None,
            [id, name] => Some((id.as_i32().unwrap(), name.as_str().unwrap().to_string())),
            _ => panic!(format!(
                "Failed to parse {}, did the structure change? {:?}",
                name, x
            )),
        },
        // Value::Array(x) => match &x[..] {
        //     [id, name] =>
        //     _ => panic!("Failed to parse parent_id, did the structure change?"),
        // },
        Value::Bool(x) if x == &false => None,
        x => panic!(format!(
            "Failed to {}, did the structure change? {:?}",
            name, x
        )),
    }
}
pub fn parse_option_string(value: &BTreeMap<String, Value>, name: &str) -> Option<String> {
    match value.get(name).unwrap_or(&Value::Nil) {
        Value::Nil => {
            //log for nothing found as a warning
            None
        }
        Value::Bool(x) if x == &false => None,
        Value::String(x) => Some(x.clone()),
        x => panic!(format!(
            "Failed to parse {}, did the structure change? {:?}",
            name, x
        )),
    }
}
pub fn parse_string(value: &BTreeMap<String, Value>, name: &str) -> String {
    match value
        .get(name)
        .expect(&format!("Failed to find {} in response", name))
    {
        Value::String(x) => x.clone(),
        x => panic!(format!(
            "Failed to {}, did the structure change? {:?}",
            name, x
        )),
    }
}

pub fn into_ids(value: Vec<i32>) -> Value {
    Value::Array(value.into_iter().map(|v| Value::Int(v)).collect())
}
pub fn into_option(value: Option<(i32, String)>) -> Value {
    match value {
        None => Value::Bool(false),
        Some(value) => Value::Array(vec![Value::Int(value.0), Value::String(value.1)]),
    }
}

pub fn into_option_string(value: Option<String>) -> Value {
    match value {
        None => Value::Bool(false),
        Some(value) => Value::String(value),
    }
}

pub trait Model {
    fn get_id(&self) -> i32;
    fn get_table_name() -> &'static str;
    fn parse(value: Value) -> Self;
    fn into_odoo_struct(self) -> Value;
}
