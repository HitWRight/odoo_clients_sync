use crate::odoo::authenticator::Authenticator;
use crate::odoo::models::version::Version;
use reqwest::RequestBuilder;
use std::collections::BTreeMap;
use std::error::Error;
use url::Url;
use xmlrpc::http::build_headers;
use xmlrpc::http::check_response;
use xmlrpc::{Request, Value};

const PATH: &str = "xmlrpc/2/common";

pub struct Connector {
    url: Url,
}

impl Connector {
    pub fn new(url: Url) -> Self {
        Connector { url }
    }

    pub fn check_version(&self) -> Version {
        let result = Request::new("version")
            .call_url(self.url.join(PATH).unwrap())
            .expect("Failed to return success code");

        if let (
            Value::Int(protocol_version),
            Value::String(server_serie),
            Value::String(server_version),
            server_version_info,
        ) = (
            result
                .get("protocol_version")
                .expect("Failed to find protocol_version in response"),
            result
                .get("server_serie")
                .expect("Failed to find server_serie in response"),
            result
                .get("server_version")
                .expect("Failed to find server_version in response"),
            result
                .get("server_version_info")
                .expect("Failed to find server_version_info in response"),
        ) {
            return Version {
                protocol_version: *protocol_version,
                server_serie: server_serie.clone(),
                server_version: server_version.clone(),
                server_version_info: server_version_info.clone(),
            };
        } else {
            panic!("Failed to parse Version info")
        }
    }

    pub fn authenticate(&self, db: &str, username: &str, password: &str) -> Authenticator {
        let empty = BTreeMap::<String, Value>::new();
        let auth_request = Request::new("authenticate")
            .arg(db)
            .arg(username)
            .arg(password)
            .arg(xmlrpc::Value::Struct(empty));

        if let Ok(Value::Int(uid)) = auth_request.call_url(self.url.join(PATH).unwrap()) {
            return Authenticator::new(self.url.clone(), db, uid, password);
        } else {
            panic!("Failed to authenticate");
        }
    }
}
