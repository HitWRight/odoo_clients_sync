mod odoo;

use crate::odoo::authenticator::Authenticator;
use aurora_api_common::models::bank::Bank;
use aurora_api_common::models::client::Client;
use odoo::models::partner::Partner;
use url::Url;
use xmlrpc::Value;

fn main() {
    println!(
        "{:?}",
        odoo::connector::Connector::new(Url::parse("http://localhost.localdomain:8069").unwrap())
            .check_version()
    );

    let authenticator =
        odoo::connector::Connector::new(Url::parse("http://localhost.localdomain:8069").unwrap())
            .authenticate("odoo2", "admin", "admin");

    // println!("{:?}", authenticator);
    // println!(
    //     "{}",
    //     clients
    //         .iter()
    //         .take(3)
    //         .map(|x| format!("{:#?}", x))
    //         .collect::<Vec<String>>()
    //         .join("\n")
    // );

    merge_banks(authenticator);

    // merge_companies(authenticator);
    // common::models::client::Client
    // let clients =

    // let partners =
    // authenticator.search_read();
    // println!("{:#?}", partners.into_iter().nth(0).unwrap().into_odoo_struct());

    // let partner = Partner::new("ukio testuolis2".to_string());
    // authenticator.create(partner);
    println!("Hello, world!");
}

fn merge_banks(auth: Authenticator) {
    let result = reqwest::blocking::get("http://127.0.0.1:8080/banks")
        .unwrap()
        .text()
        .unwrap();

    let banks: Vec<Bank> = serde_json::from_str(&result).unwrap();
    println!("{:?}", banks);

    let odoo_banks: Vec<crate::odoo::models::bank::Bank> =
        auth.search_read(Value::Array(vec![Value::Array(vec![Value::Array(vec![
            Value::Bool(true),
            Value::String("=".to_string()),
            Value::Bool(true),
        ])])]));
    println!("{:?}", odoo_banks);

    for bank in &banks {
        match odoo_banks.iter().find(|x| x.name == bank.name) {
            Some(x) => edit_bank(&auth, x, bank),
            None => create_bank(&auth, bank),
        }
    }

    let odoo_banks: Vec<crate::odoo::models::bank::Bank> =
        auth.search_read(Value::Array(vec![Value::Array(vec![Value::Array(vec![
            Value::Bool(true),
            Value::String("=".to_string()),
            Value::Bool(true),
        ])])]));

    for bank in odoo_banks {
        match banks.iter().find(|x| x.name == bank.name) {
            Some(_) => (),
            None => delete_bank(&auth, bank)
        }
    }

}

fn delete_bank(authenticator: &Authenticator, bank: crate::odoo::models::bank::Bank) {
    println!("Deleting {:#?}", bank);
    authenticator.delete(bank);
}

fn create_bank(authenticator: &Authenticator, bank: &Bank) {
    let new_bank = odoo::models::bank::Bank::new(bank.name.clone());
    authenticator.create(new_bank);
}

fn edit_bank(authenticator: &Authenticator, current_bank: &crate::odoo::models::bank::Bank, bank: &Bank) {
    let mut current_bank = current_bank.clone();
    current_bank.name = bank.name.clone();
    authenticator.write(current_bank);
}

// fn merge_companies(auth: Authenticator) {
//     let result = reqwest::blocking::get("http://127.0.0.1:8080/clients")
//         .unwrap()
//         .text()
//         .unwrap();
//     let clients: Vec<Client> = serde_json::from_str(&result).unwrap();
//     //get aurora companies
//     println!("Client count: {}", clients.len());

//     let partners: Vec<Partner> =
//         auth.search_read(Value::Array(vec![Value::Array(vec![Value::Array(vec![
//             Value::String("is_company".to_string()),
//             Value::String("=".to_string()),
//             Value::Bool(true),
//         ])])]));

//     println!("Partners count: {}", partners.len());

//     for client in clients {
//         match partners.iter().find(|x| match &x.registration_id {
//             Some(x) => client.registration_id == *x,
//             None => false,
//         }) {
//             Some(_partner) => (),
//             None => create_company(&auth, client),
//         }
//     }

//     // println!("{:#?}", partners);

//     // let filter = Value::Array(vec![
//     //     // Value::Array(vec![Value::Array(vec![
//     //     // Value::String("is_company".to_string()),
//     //     // Value::String("=".to_string()),
//     //     // Value::Bool(true),
//     //     // ])])
//     // ]);

//     //get odoo companies
//     //find diff
//     //priority for aurora companies, delete odoo companies
// }

// fn create_company(authenticator: &Authenticator, client: Client) {
//     println!("Filling out company {:?}", client);
//     let mut new_partner = Partner::new(client.name);
//     new_partner.is_company = true;
//     // new_partner.company_type = "Company".to_string();
//     new_partner.aurora_id = client.id;
//     // TODO: Add bank merge
//     // new_partner.bank_ids = bank
//     new_partner.comment = client.comment;
//     new_partner.email = client.email.unwrap_or("".to_string());
//     // TODO: Add industry merge
//     // new_partner.industry_id = organization_type
//     new_partner.phone = client.phone.unwrap_or("".to_string());
//     new_partner.registration_id = Some(client.registration_id);
//     new_partner.vat = client.vat_id;
//     new_partner.vat_regression = client.vat_regression;
//     authenticator.create(new_partner);
// }

// // fn delete_company()

// fn merge_company_contacts() {
//     //get aurora contacts
//     //get odoo contacts
//     //find diff
//     //priority for aurora contacts, delete odoo contacts
// }
